const electron = require('electron')
const url = require('url')
const path = require('path')

const {app, BrowserWindow} = electron

let mainWindow

app.commandLine.appendSwitch("--disable-http-cache")

app.on('ready', () => {
	mainWindow = new BrowserWindow({
		frame: false,
		closable: false,
		movable: false,
		resizable: false,
		fullscreen: true,
		alwaysOnTop: true,
		kiosk: true
	})
	mainWindow.loadURL('http://localhost:3000/', { extraHeaders: 'pragma: no-cache\n' })

	mainWindow.webContents.on('did-finish-load', () => {
		mainWindow.show();
		mainWindow.focus();
		mainWindow.webContents.send('app.started');
	});

	mainWindow.on('closed', () => {
		mainWindow = null;
	});
})